package com.gitlab.vinnystalck.equipability;

import com.gitlab.vinnystalck.equipability.accessories.TotemOfUndyingAccessory;
import com.mojang.logging.LogUtils;
import org.slf4j.Logger;

public class Equipability {

    public static final String MOD_ID = "equipability";

    public static final Logger LOGGER = LogUtils.getLogger();

    /**
     * Initializes the mod.
     */
    public static void init() {
        TotemOfUndyingAccessory.init();
    }
}
