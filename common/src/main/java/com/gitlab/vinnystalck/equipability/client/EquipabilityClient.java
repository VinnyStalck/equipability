package com.gitlab.vinnystalck.equipability.client;

import com.gitlab.vinnystalck.equipability.accessories.TotemOfUndyingAccessory;

/**
 * EquipabilityClient
 */
public class EquipabilityClient {
    public static void init() {
        TotemOfUndyingAccessory.clientInit();
    }
}