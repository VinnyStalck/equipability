package com.gitlab.vinnystalck.equipability.fabric;

import com.gitlab.vinnystalck.equipability.Equipability;
import net.fabricmc.api.ModInitializer;

/**
 * This class is the entrypoint for the mod on the Fabric platform.
 */
public class EquipabilityFabric implements ModInitializer {

    @Override
    public void onInitialize() {
        Equipability.init();
    }
}
