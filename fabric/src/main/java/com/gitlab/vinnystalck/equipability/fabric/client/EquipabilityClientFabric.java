package com.gitlab.vinnystalck.equipability.fabric.client;

import com.gitlab.vinnystalck.equipability.client.EquipabilityClient;

import net.fabricmc.api.ClientModInitializer;

public class EquipabilityClientFabric implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        EquipabilityClient.init();
    }
}
