package com.gitlab.vinnystalck.equipability.neoforge;

import com.gitlab.vinnystalck.equipability.Equipability;
import net.neoforged.fml.common.Mod;


/**
 * Main class for the mod on the NeoForge platform.
 */
@Mod(Equipability.MOD_ID)
public class EquipabilityNeoForge {
    public EquipabilityNeoForge() {
        Equipability.init();
    }
}
