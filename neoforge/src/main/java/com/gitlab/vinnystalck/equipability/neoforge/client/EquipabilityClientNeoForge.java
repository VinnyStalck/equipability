package com.gitlab.vinnystalck.equipability.neoforge.client;

import com.gitlab.vinnystalck.equipability.Equipability;
import com.gitlab.vinnystalck.equipability.client.EquipabilityClient;

import net.neoforged.api.distmarker.Dist;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.event.lifecycle.FMLClientSetupEvent;

/**
 * EquipabilityClientNeoForge
 */
@EventBusSubscriber(modid = Equipability.MOD_ID, value = Dist.CLIENT, bus = EventBusSubscriber.Bus.MOD)
public class EquipabilityClientNeoForge {
    @SubscribeEvent
    public static void onInitializeClient(FMLClientSetupEvent event) {
        EquipabilityClient.init();
    }
}